Complete the table below with your results, and then provide your interpretation at the end.

Note that:

- When calculating the parallel speed-up S, use the time output by the code, which corresponds
  to the parallel calculation and does not include reading in the file or performing the serial check.

- Take as the serial execution time the time output by the code when run with a single process
  (hence the speed-up for 1 process must be 1.0, as already filled in the table).


No. Process:                        Mean time (average of 3 runs)           Parallel speed-up, S:
===========                         ============================:           ====================
1                                   0.00043592								1.0 
2                                   0.00030067                              1.44983
4                                   0.00892679                              0.04883

Architecture that the timing runs were performed on:
Ubuntu 20.04-64 Bit Virtual Box
AMD RYZEN 4700U 4 Cores made available to virtual machine 1 Thread per core
8GB DDR4 RAM

A brief interpretation of these results (2-3 sentences should be enough):
We see a parallel increase when running the program with 2 processes instead of one. We can interpret this as the computation time has been reduced by splitting the workload over 2 threads more than the communication time added for the communication between the two threads.

When 4 processes are used we have greatly reduced the performance of the program. This can be interpreted as the communication time between 4 threads is much greater than the compuatational reduction from splitting the workload over 4 threads for this workload size this could be due to the system only having 4 cores.I belive as the workload size increases we would start to see a parallel speedup. 
