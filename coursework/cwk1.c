//
// COMP3221 Parallel Computation: OpenMP.
//


//
// Includes.
//

// Standard includes.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// The OMP library.
#include <omp.h>

// The include file for this coursework. You may inspect this file, but should NOT alter it, as it will be replaced
// with a different version for assessment.
#include "cwk1_extra.h"

// Key variables and functions in "cwk1_extra.h":
// - each entry in the data array is one of the following structs:
//   typedef struct Entry
//   {
//     char *name;
//     int id;
//   } Entry_t;
// - the pointer to the array, and the number of entries within it, are global:
//  Entry_t *orderedData;
//  int dataSize;
//
// int loadOrderedData() loads the data from "orderedData.txt" and returns 0 for success.
// void printData() displays the full data array to stdout.
// void deleteOrderedData() performs all required operations on the data array before quitting program.
//
// void swapEntries( int i, int j )
// - swaps the entries with indices i and j. Prints an error message if i==j, or if either index is out of range.
//   Uses local temporary variables. Not thread safe.
//
// int randomEntryIndex()
// - returns a random entry index, i.e. a random integer in the range 0 to dataSize-1 inclusive. Not thread safe.


//
// The following 4 functions correspond to each of the operations. It is expected you will add fill in each of these functions
// with your solution. You can also add other functions to this file if you like, but if you decide to add a new file, make
// sure to follow the coursework instructions for submission.
//

// Reverses the order of the data in-place, i.e. leaving the answer in the same array as before.
/*
 * For this function I have calculated the number of swaps required and made sure this number is an integer
 * Although dividing dataSize by 2 would give the same value due to integer division its clearer to explicitly calculate
 * the number of swaps.
 * I then loop through and swap the entries using the provided function. This can be parallelised as there is no data
 * dependices between threads.*/
void reverseOrder_inParallel()
{
	int swaps; 
	//Calculate the number of swaps we need
	if(dataSize % 2){
		//Make sure the number of iterations is an integer.
		swaps = (dataSize - 1) / 2;
	}

	//Swap the relevant entries
	#pragma omp parallel for
	for(int i=0;i<swaps;i++){
		swapEntries(i,dataSize-i-1);
	}

}
/*Sort the entries in place using odd-even bubble sort
 * Complexity O(n^2)
 * Space Complexity O(1)
 */
// Sort all entries in-place, in order of increasing id.
void sortByID_inParallel()
{
	int swaps=1;
	int startIndex=0;
	while(swaps)
	{
		//Set the swap toggle
		swaps=0;
		#pragma omp parallel for 
		for(int j=startIndex;j<dataSize-1;j=j+2){
			if(orderedData[j].id > orderedData[j+1].id){
				swapEntries(j,j+1);
				//Carry on checking
				swaps=1;
			}
		}
		//Toggle between odd and even cycles
		startIndex = !startIndex;
	}

 
}

/*Here I have used the rand_r function to generate random numbers each with a unique seed
 *I then swap the entries in a critical region
 */
void shuffle_inParallel()
{
	//Get an initial seed 
	unsigned int seed = time(NULL);
	//Create an array that holds all the thread seeds
	unsigned int thread_seeds[omp_get_max_threads()];
	//Calculate the seeds for each thread
	#pragma omp parallel for
	for(int i=0;i<omp_get_max_threads();i++){
		thread_seeds[i] = seed + i;
	}
	#pragma omp parallel for
	for(int i=0;i<(dataSize*(dataSize-1)/2);i++){
		//Get the current thread number
		int threadNum = omp_get_thread_num();
		//P and  Q are private for each thread
		int p,q;
		//Generate 2 random numbers using the thread seeds
		p = rand_r(&thread_seeds[threadNum]) % dataSize;
		q = rand_r(&thread_seeds[threadNum]) % dataSize;
		if( p != q){
				//Swap the entries once at a time
				#pragma omp critical
				{
				swapEntries(p,q);
				}
		}
	}

}

// Remove the last item from the list in a thread-safe manner. You do not need to re-allocate any memory for the data array.
void removeLastItem_threadSafe()
{
	#pragma omp critical
	{
		//Check the list is not empty
		if(dataSize > 0){
			dataSize = dataSize - 1;
		}
	}
}



//
// You should not modify the code in main(), but should understand how it works.
//
int main( int argc, char **argv )
{
    // Initialise the random number generator to the system clock.
    srand( time(NULL) );

    //
    // Parse command line arguments. Requires an option number to be entered.
    //

    // Make sure we have exactly 1 command line argument (which, plus the executable name, means 'argc' should be exactly 2).
    if( argc != 2 )
    {
        printf( "Enter a single command line argument for the operation required:\n(1) Reverse the order.\n(2) Sort in order of increasing ID.\n" );
        printf( "(3) Shuffle.\n(4) Remove all items from the end in a parallel loop.\n" );
        return EXIT_FAILURE;
    }

    // Convert to an option number, and ensure it is in the valid range. Note argv[0] is the executable name.
    int option = atoi( argv[1] );
    if( option<=0 || option>4 )
    {
        printf( "Option number '%s' invalid.\n", argv[1] );
        return EXIT_FAILURE;
    }

    // Display how many threads we are using, if only to confirm this is actually in parallel.
    printf( "Performing option '%i' using %i OpenMP thread(s).\n\n", option, omp_get_max_threads() );

    //
    // Initialise the data set.
    //

    // Loads the data from file. loadOrderedData() (defined in "cwk1_extra.h" returns a non-negative integer
    // if successful, otherwise it will display an error message and return a negative integer.
    if( loadOrderedData()<0 ) return EXIT_FAILURE;

    // Print the initial ordered data to screen. printData() is defined in "cwk1_extra.h".
    printf( "Before the operation:\n" );
    printData();

    //
    // Perform an operation on the data depending on the option entered on the command line.
    //
    int i, initialDataSize = dataSize;

    switch( option )
    {
        case 1:
            reverseOrder_inParallel();
            break;
        
        case 2:
            sortByID_inParallel();
            break;

        case 3:
            shuffle_inParallel();
            break;

        case 4:
            #pragma omp parallel for
            for( i=0; i<initialDataSize; i++ )
                removeLastItem_threadSafe();
            break;

        default:

            // Shouldn't be possible to reach here given the earlier checks.
            printf( "Option '%i' not implememnted by the switch() statement.\n", option );
            return EXIT_FAILURE;
    }

    //
    // Print the data after the operation, then free up all resources and quit.
    //
    printf( "\nAfter the operation:\n" );
    printData();

    // You MUST call this function from "cwk1_extra.h" (which should be unmodifed) just prior to quitting your program.
    deleteOrderedData();

    return EXIT_SUCCESS;
}
